#include <stdio.h>
#include <string.h>

// Deklarasi fungsi
void inputData(char *nama, double *quiz, double *absen, double *uts, double *uas, double *tugas);
double hitungNilai(double quiz, double absen, double uts, double uas, double tugas);
void cetakMutu(double nilai);

int main() {

  int jumlahMhs;
  printf("Masukkan jumlah mahasiswa: ");
  scanf("%d", &jumlahMhs);

  char nama[100];
  double quiz, absen, uts, uas, tugas, nilai;
  char hurufMutu;

  for(int i=0; i<jumlahMhs; i++) {

    // Input data mahasiswa
    inputData(nama, &quiz, &absen, &uts, &uas, &tugas);
    
    // Hitung nilai akhir
    nilai = hitungNilai(quiz, absen, uts, uas, tugas);
    
    // Cetak mutu
    cetakMutu(nilai);
    
    
  }

  return 0;
}

// Fungsi input data mahasiswa
void inputData(char *nama, double *quiz, double *absen, double *uts, double *uas, double *tugas) {

  char namaMhs[100];
  
  printf("Masukkan nama: ");
  scanf("%s", namaMhs);

  strcpy(nama, namaMhs);

  printf("Masukkan nilai quiz: ");
  scanf("%lf", quiz);

  printf("Masukkan nilai absen: ");  
  scanf("%lf", absen);

  printf("Masukkan nilai UTS: ");
  scanf("%lf", uts);

  printf("Masukkan nilai UAS: ");
  scanf("%lf", uas);

  printf("Masukkan nilai tugas: ");
  scanf("%lf", tugas);

}

// Fungsi hitung nilai
double hitungNilai(double quiz, double absen, double uts, double uas, double tugas) {

  double nilai;

  nilai = (0.1 * absen) + (0.2 * tugas) + (0.3 * quiz) + (0.4 * uts) + (0.5 * uas);

  return nilai;

}

// Procedure cetak mutu
void cetakMutu(double nilai) {

  char hurufMutu;

  if (nilai > 85)
    hurufMutu = 'A';
  else if (nilai > 70)
    hurufMutu = 'B';
  else if (nilai > 55)
    hurufMutu = 'C';
  else if (nilai > 40)
    hurufMutu = 'D';
  else 
    hurufMutu = 'E';

  printf("Huruf Mutu: %c\n\n", hurufMutu);

}
